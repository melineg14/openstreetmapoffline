import QtQuick 2.12
import QtQuick.Window 2.2


import "Widgets"

Window {
    width: 1300//640
    height: 700//480
    visible: true
    title: qsTr("OpenStreetMapOffline")

    Rectangle
    {
        width: parent.width
        height: parent.height * 1/5
        color: "lightgreen"
    }

    Rectangle
    {
        y: parent.height * 1/5
        width: parent.width * 1/3
        height: parent.height * 4/5
        color: "lightgrey"
    }

    Item {
        id: mapArea
        x: parent.width * 1/3
        y: parent.height * 1/5
        width: parent.width * 2/3
        height: parent.height * 4/5


        NavMap{width: parent.width; height: parent.height}

//        Plugin {
//                id: mapPlugin
//                name: "osm"
//                 PluginParameter {
//                     name: 'osm.mapping.offline.directory'
//                     value: ':/offline_tiles/'
//                 }
//            }

//        Map {
//            id: map
//            anchors.fill: parent
//            activeMapType: map.supportedMapTypes[3]
//            zoomLevel: 8
//            plugin: mapPlugin
//            center: QtPositioning.coordinate(43.529742, 5.447427) // Aix
//        }
    }
}

import QtQuick 2.12
import QtPositioning 5.8
import QtLocation 5.15

Item {
    id: mapItem
    width: mapItem.width
    height: mapItem.height
    x: mapItem.x
    y: mapItem.y

    Plugin {
        id: mapPlugin
        name: "osm"
         PluginParameter {
             name: 'osm.mapping.offline.directory'
//           value: '/home/mel/work/AH/OpenStreetMapOffline/OpenStreetMapOffline/offline_tiles/'
             value: 'file:///offline_tiles/'

         }
//         PluginParameter {
//             name: "osm.mapping.custom.host"
//             value: "file:///home/mel/work/AH/OpenStreetMapOffline/OpenStreetMapOffline/offline_osm_tiles/"
//           value: "file:///home/mel/T%C3%A9l%C3%A9chargements/Maperitive/Tiles/"
//           value: 'qrc:/offline_tiles/'
//         }
//         PluginParameter {
//             name: "osm.osm.mapping.cache.directory"
//             value: ':/offline_tiles/'
//         }
//         PluginParameter {
//             name: "osm.mapping.providersrepository.disabled"
//             value: true
//         }
    }

    Map {
        id: map
        anchors.fill: parent
        activeMapType: /*MapType.CustomMap*/ map.supportedMapTypes[2]
        zoomLevel: 10
        plugin: mapPlugin
        center: QtPositioning.coordinate(43.529742, 5.447427) // Aix
//        gesture.acceptedGestures: MapGestureArea.PanGesture | MapGestureArea.FlickGesture | MapGestureArea.PinchGesture | MapGestureArea.RotationGesture | MapGestureArea.TiltGesture
//        gesture.flickDeceleration: 3000
//        gesture.enabled: true

//        Component.onCompleted: {
//            for( var i_type in supportedMapTypes ) {
//                if( supportedMapTypes[i_type].name.localeCompare( "file:///home/mel/work/AH/OpenStreetMapOffline/OpenStreetMapOffline/offline_osm_tiles/" ) === 0 ) {
//                    activeMapType = supportedMapTypes[i_type]
//                }
//            }
//        }

    }

//    PositionSource {
//        onPositionChanged: {
//            map.center = position.coordinate
//        }
//    }
}

#ifndef TILESFILESCONFIGURATION_H
#define TILESFILESCONFIGURATION_H

//#include <QDir>
#include <QString>

class TilesFilesConfiguration
{
    public:
        TilesFilesConfiguration();
        ~TilesFilesConfiguration();
//        QDir getTilesDir();
        QString getTileFileName();


    private:
        QString m_zArgument;
        QString m_xArgument;
        QString m_yArgument;
//        std::string m_zArgument;
//        std::string m_xArgument;
//        std::string m_yArgument;


};
#endif // TILESFILESCONFIGURATION_H

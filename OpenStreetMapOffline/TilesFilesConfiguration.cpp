#include "TilesFilesConfiguration.h"
#include <QDir>
#include <QCoreApplication>
#include <QString>
#include <iostream>
#include <QDebug>

TilesFilesConfiguration::TilesFilesConfiguration()
{
}

TilesFilesConfiguration::~TilesFilesConfiguration()
{
}

QString TilesFilesConfiguration::getTileFileName()
{
    QString applicationDirPath = QCoreApplication::applicationDirPath();

    QString tilesDirectoryPath = applicationDirPath + "/offline_OSM_tiles";

    QDir tilesDirectory;

    if(tilesDirectory.cd(tilesDirectoryPath))
    {
        m_zArgument = tilesDirectory.dirName();
    }

    if(tilesDirectory.cd(tilesDirectoryPath + "/" + m_zArgument))
    {
        m_xArgument = tilesDirectory.dirName();
        QFile tileFile(tilesDirectoryPath + "/" + m_zArgument);
        m_yArgument =  tileFile.fileName();
    }

    QString tileFileName = "osm_100-l-3-" + m_zArgument + "-" + m_xArgument + "-" + m_yArgument;

    return tileFileName;
}
